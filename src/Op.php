<?php

namespace pqAsync;

/**
 * Class Op
 *
 * @package pqAsync
 */
class Op
{
    use EventEmitter { trigger as public; }

    /**
     * @var callable
     */
    public $target;

    /**
     * @var array
     */
    public $args;

    /**
     * @var mixed
     */
    public $retVal;

    /**
     * @var bool
     */
    public $cancelled = false;

    /**
     * @var bool
     */
    public $executing = false;

    /**
     * @var bool
     */
    public $autoComplete = false;

    /**
     * @param callable $target
     * @param array $args
     * @param bool $autoComplete
     */
    public function __construct($target, array $args, $autoComplete = false)
    {
        $this->target = $target;
        $this->args = $args;
        $this->autoComplete = $autoComplete;
    }
}
