<?php

namespace pqAsync;

use Amp\Reactor;
use pq\Connection as pqConnection;
use pq\Cancel;
use pq\Result;

/**
 * Class Query
 *
 * @package pqAsync
 */
class Query
{
    use EventEmitter;

    const STATUS_PENDING = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_ENDED = 2;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var Op
     */
    private $op;

    /**
     * @var Reactor
     */
    private $reactor;

    /**
     * @var pqConnection
     */
    private $pqConnection;

    /**
     * @var callable
     */
    private $pollCycleCallback;

    /**
     * @var int
     */
    private $status = self::STATUS_PENDING;

    /**
     * @var bool
     */
    private $inResultSet = false;

    /**
     * @param Connection $connection
     * @param Op $op
     */
    public function __construct(Connection $connection, Op $op)
    {
        $this->connection = $connection;
        $this->op = $op;

        $this->reactor = $connection->getReactor();
        $this->pqConnection = $connection->getPQConnection();

        $this->pollCycleCallback = function(Reactor $reactor, $watcherId) {
            $reactor->cancel($watcherId);

            $this->pqConnection->poll();

            if ($this->pqConnection->busy) {
                $this->reactor->onReadable($this->pqConnection->socket, $this->pollCycleCallback);
                return;
            }

            $this->processResult();
        };

        $op->on('ready', function() {
            $this->trigger('response.start');

            $this->status = self::STATUS_ACTIVE;
            $this->processResult();
        });
    }

    private function processResult()
    {
        static $simpleEvents = [
            Result::BAD_RESPONSE   => 'error.server',
            Result::EMPTY_QUERY    => 'error.empty',
            Result::FATAL_ERROR    => 'error.fatal',
            Result::NONFATAL_ERROR => 'error.nonfatal',
            Result::COPY_IN        => 'copy.in',
            Result::COPY_OUT       => 'copy.out',
        ];

        do {
            $result = $this->pqConnection->getResult();

            if ($result === null) {
                $this->status = self::STATUS_ENDED;
                $this->trigger('response.end');

                $this->op->trigger('complete', $this->op);

                return;
            } else if (isset($simpleEvents[$result->status])) {
                $this->trigger($simpleEvents[$result->status], $result);
            } else if (Result::COMMAND_OK === $result->status) {
                if (!$this->inResultSet) {
                    $this->trigger('resultset.start', $result);
                    $this->trigger('resultset.end', $result);
                }
            } else if (Result::SINGLE_TUPLE === $result->status) {
                if (!$this->inResultSet) {
                    $this->trigger('resultset.start', $result);
                    $this->inResultSet = true;
                }

                $this->trigger('resultset.row', $result);
            } else if (Result::TUPLES_OK === $result->status) {
                if (!$this->inResultSet) {
                    $this->trigger('resultset.start', $result);
                    $this->trigger('resultset.row', $result);
                }

                $this->trigger('resultset.end', $result);
                $this->inResultSet = false;
            }
        } while (!$this->pqConnection->busy);

        $this->reactor->onReadable($this->pqConnection->socket, $this->pollCycleCallback);
    }

    /**
     * @return Connection
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function cancel()
    {
        if ($this->status === self::STATUS_ENDED) {
            throw new \BadMethodCallException('Cannot cancel a query that has already been processed');
        }

        if ($this->status === self::STATUS_ACTIVE || $this->op->executing) {
            (new Cancel($this->pqConnection))->cancel();
        }
        $this->op->cancelled = true;

        $status = $this->status;
        $this->status = self::STATUS_ENDED;

        $this->trigger('query.cancel', $status);
        $this->op->trigger('complete', $this->op);
    }
}
