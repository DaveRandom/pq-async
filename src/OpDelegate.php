<?php

namespace pqAsync;

use pq\Connection as pqConnection;

trait OpDelegate
{
    /**
     * @param Op $rootOp
     * @param callable $method
     * @param array $args
     * @param bool $autoComplete
     * @return Op
     */
    private function enqueueOp(Op $rootOp, callable $method, array $args, $autoComplete = false)
    {
        return $rootOp->trigger('op.enqueue', $method, $args, $autoComplete);
    }

    /**
     * @param pqConnection $connection
     * @param Op $op
     */
    private function clearResultsAndCompleteOp(pqConnection $connection, Op $op)
    {
        while ($connection->getResult());
        $op->trigger('complete');
    }
}
