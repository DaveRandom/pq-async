<?php

namespace pqAsync;

/**
 * Trait EventEmitter
 *
 * @package pqAsync
 */
trait EventEmitter
{
    /**
     * @var int
     */
    private $watcherIdCounter = 0;

    /**
     * @var string[]
     */
    private $watcherIdMap = [];

    /**
     * @var array
     */
    private $watcherCallbacks = [];

    /**
     * @return int
     */
    private function getNextWatcherId()
    {
        do {
            $id = $this->watcherIdCounter++;

            if ($this->watcherIdCounter > PHP_INT_MAX) {
                $this->watcherIdCounter = ~PHP_INT_MAX;
            }
        } while(isset($this->watcherIdMap[$id]));

        return $id;
    }

    /**
     * Trigger an event
     *
     * @param string $eventName
     * @param mixed $args
     * @return bool
     */
    protected function trigger($eventName, ...$args)
    {
        if (!empty($this->watcherCallbacks['*'])) {
            foreach ($this->watcherCallbacks['*'] as $callback) {
                $callback($eventName, ...$args);
            }
        }

        if (!empty($this->watcherCallbacks[$eventName])) {
            foreach ($this->watcherCallbacks[$eventName] as $id => $callback) {
                if (null !== $retVal = $callback(...$args, ...[$this, $id])) {
                    return $retVal;
                }
            }
        }

        return null;
    }

    /**
     * Register an event handler
     *
     * @param string $eventName
     * @param callable
     * @return int
     */
    public function on($eventName, callable $callback)
    {
        $id = $this->getNextWatcherId();

        $this->watcherIdMap[$id] = $eventName;
        $this->watcherCallbacks[$eventName][$id] = $callback;

        return $id;
    }

    /**
     * De-register an event handler
     *
     * @param int $id
     */
    public function off($id)
    {
        if (!isset($this->watcherIdMap[$id])) {
            throw new \InvalidArgumentException('Invalid watcher ID: ' . $id);
        }

        $eventName = $this->watcherIdMap[$id];
        unset($this->watcherCallbacks[$eventName][$id], $this->watcherIdMap[$id]);
    }
}
