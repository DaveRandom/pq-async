<?php

namespace pqAsync;

use Amp\Reactor;
use pq\Connection as pqConnection;
use pq\Statement as pqStatement;
use pq\Result as pqResult;

/**
 * Class Statement
 *
 * @package pqAsync
 */
class Statement
{
    use EventEmitter;
    use OpDelegate;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var Op
     */
    private $rootOp;

    /**
     * @var Reactor
     */
    private $reactor;

    /**
     * @var pqConnection
     */
    private $pqConnection;

    /**
     * @var pqStatement
     */
    private $pqStatement;

    /**
     * @var int[]
     */
    private $oids;

    /**
     * @var int
     */
    private $oidStatus = 0;

    /**
     * @var callable
     */
    private $oidCallbacks = [];

    /**
     * @param Connection $connection
     * @param Op $op
     */
    public function __construct(Connection $connection, Op $op)
    {
        $this->connection = $connection;
        $this->rootOp = $op;

        $this->reactor = $connection->getReactor();
        $this->pqConnection = $connection->getPQConnection();

        $op->on('start', function(Op $op) {
            $this->pqStatement = $op->retVal;
        });
        $op->on('ready', function(Op $op) {
            $result = $this->pqConnection->getResult();

            if (pqResult::COMMAND_OK === $result->status) {
                $this->trigger('prepare.success');
            } else {
                $this->trigger('prepare.fail', new \Exception($result->errorMessage));
            }

            $this->clearResultsAndCompleteOp($this->pqConnection, $op);
        });
    }

    /**
     * @return Connection
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @return pqStatement
     */
    public function getPQStatement()
    {
        return $this->pqStatement;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->pqStatement ? $this->pqStatement->name : $this->rootOp->args[0];
    }

    /**
     * @param int $paramNo
     * @param mixed $paramRef
     */
    public function bind($paramNo, &$paramRef = null)
    {
        $this->pqStatement->bind($paramNo, $paramRef);
    }

    /**
     * @param callable $callback
     */
    public function describe(callable $callback)
    {
        if ($this->oidStatus === 2) {
            $this->reactor->immediately(function() use($callback) {
                $callback($this->oids);
            });

            return;
        }

        $this->oidCallbacks[] = $callback;

        if ($this->oidStatus === 1) {
            return;
        }

        $this->oidStatus = 1;

        $method = [$this->pqStatement, 'descAsync'];
        $args = [function(pqResult $result) {
            $this->oidStatus = 2;
            $this->oids = $result->desc();

            foreach ($this->oidCallbacks as $callback) {
                $callback($this->oids);
            }
        }];
        $autoComplete = true;

        $this->enqueueOp($this->rootOp, $method, $args, $autoComplete);
    }

    /**
     * @param array $params
     * @return query
     */
    public function exec(array $params = null)
    {
        $method = [$this->pqStatement, 'execAsync'];
        $args = [$params];
        $autoComplete = false;

        return new Query($this->connection, $this->enqueueOp($this->rootOp, $method, $args, $autoComplete));
    }
}
