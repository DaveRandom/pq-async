<?php

namespace pqAsync;

use Amp\Reactor;
use pq\Connection as pqConnection;

/**
 * Class Connection
 *
 * @package pqAsync
 */
class Connection
{
    use EventEmitter;

    const OPTION_PERSISTENT                     = 1;
    const OPTION_UNBUFFERED                     = 2;
    const OPTION_ENCODING                       = 3;
    const OPTION_DEFAULT_FETCH_TYPE             = 4;
    const OPTION_DEFAULT_AUTO_CONVERT           = 5;
    const OPTION_DEFAULT_TRANSACTION_DEFERRABLE = 6;
    const OPTION_DEFAULT_TRANSACTION_ISOLATION  = 7;
    const OPTION_DEFAULT_TRANSACTION_READ_ONLY  = 8;

    /**
     * @var Reactor
     */
    private $reactor;

    /**
     * @var pqConnection
     */
    private $pqConnection;

    /**
     * @var callable[]
     */
    private $callbacks = [];

    /**
     * @var bool
     */
    private $connected = false;

    /**
     * @var bool
     */
    private $persistent = false;

    /**
     * @var \SplQueue
     */
    private $opQueue;

    /**
     * @var Op
     */
    private $currentOp;

    /**
     * @var string[]
     */
    private $subscribedChannels = [];

    /**
     * @var Op[]
     */
    private $pendingListenChannels = [];

    /**
     * @var Op[]
     */
    private $pendingUnlistenChannels = [];

    /**
     * @var int[]
     */
    private $watcherIds = [];

    /**
     * @var string[]
     */
    private static $optionPropertyMap = [
        self::OPTION_UNBUFFERED                     => 'unbuffered',
        self::OPTION_ENCODING                       => 'encoding',
        self::OPTION_DEFAULT_FETCH_TYPE             => 'defaultFetchType',
        self::OPTION_DEFAULT_AUTO_CONVERT           => 'defaultAutoConvert',
        self::OPTION_DEFAULT_TRANSACTION_DEFERRABLE => 'defaultTransactionDeferrable',
        self::OPTION_DEFAULT_TRANSACTION_ISOLATION  => 'defaultTransactionIsolation',
        self::OPTION_DEFAULT_TRANSACTION_READ_ONLY  => 'defaultTransactionReadonly',
    ];

    /**
     * @param Reactor $reactor
     * @param string $dsn
     * @param array $options
     */
    public function __construct(Reactor $reactor, $dsn = null, array $options = [])
    {
        $this->reactor = $reactor;
        $this->opQueue = new \SplQueue;

        $this->createCallbacks();

        if (isset($dsn)) {
            $this->connect($dsn, $options);
        }
    }

    private function createCallbacks()
    {
        $this->callbacks['connect_activity'] = function(Reactor $reactor, $watcherId) {
            static $methods = [
                pqConnection::POLLING_READING => 'onReadable',
                pqConnection::POLLING_WRITING => 'onWritable',
            ];

            $reactor->cancel($watcherId);

            $status = $this->pqConnection->poll();

            if (isset($methods[$status])) {
                $reactor->{$methods[$status]}($this->pqConnection->socket, $this->callbacks['connect_activity']);
                return;
            }

            unset($this->callbacks['connect_activity']);

            if (pqConnection::POLLING_FAILED === $status) {
                $this->trigger('connect.fail', new \Exception($this->pqConnection->errorMessage));
                return;
            }

            $this->connected = true;
            $this->trigger('connect.success');

            if ($this->opQueue->count()) {
                $this->scheduleProcessNextOp();
            }
        };

        $this->callbacks['op_readable'] = function(Reactor $reactor, $watcherId) {
            $this->pqConnection->poll();

            if (!$this->pqConnection->busy) {
                $reactor->cancel($watcherId);
                $this->watcherIds['op_readable'] = null;

                $this->currentOp->trigger('ready', $this->currentOp);

                if ($this->currentOp && $this->currentOp->autoComplete) {
                    $this->clearResultsAndCompleteCurrentOp();
                }
            }
        };

        $this->callbacks['op_writable'] = function(Reactor $reactor, $watcherId) {
            $reactor->cancel($watcherId);
            $this->watcherIds['op_writable'] = null;

            $this->processNextOp();
        };

        $this->callbacks['idle_readable'] = function(Reactor $reactor, $watcherId) {
            $this->pqConnection->poll();
            while ($this->pqConnection->getResult());

            if (!$this->subscribedChannels) {
                $this->watcherIds['idle_readable'] = null;
                $reactor->cancel($watcherId);
            }
        };

        $this->callbacks['op_complete'] = function() {
            $this->currentOp->executing = false;
            $this->trigger('op.end', $this->currentOp);
            $this->currentOp = null;

            if ($this->opQueue->count()) {
                $this->scheduleProcessNextOp();
            } else {
                $this->handleEmptyOpQueue();
            }
        };

        $this->callbacks['op_request_handler'] = function($method, $args, $autoComplete) {
            return $this->enqueueOp($method, $args, $autoComplete);
        };

        $this->callbacks['channel_message_handler'] = function($channel, $message, $srcPID) {
            $this->triggerChannelEvent($channel, 'message', $message, $srcPID);
        };
    }

    private function clearResultsAndCompleteCurrentOp()
    {
        while ($this->pqConnection->getResult());

        $this->reactor->immediately(function() {
            $this->currentOp->trigger('complete');
        });
    }

    private function scheduleProcessNextOp()
    {
        if (!isset($this->watcherIds['op_writable'])) {
            $this->watcherIds['op_writable'] = $this->reactor->onWritable($this->pqConnection->socket, $this->callbacks['op_writable']);
        }
    }

    private function handleEmptyOpQueue()
    {
        $this->trigger('queue.drain');

        if ($this->subscribedChannels) {
            $this->watcherIds['idle_readable'] = $this->reactor->onReadable($this->pqConnection->socket, $this->callbacks['idle_readable']);
        }
    }

    /**
     * @param callable $method
     * @param array $args
     * @param bool $autoComplete
     * @return Op
     */
    private function enqueueOp(callable $method, array $args = [], $autoComplete = false)
    {
        $op = new Op($method, $args, $autoComplete);
        $op->on('complete', $this->callbacks['op_complete']);
        $op->on('op.enqueue', $this->callbacks['op_request_handler']);

        $this->opQueue->enqueue($op);
        $this->trigger('op.enqueue', $op);

        if ($this->connected && !$this->currentOp) {
            if (isset($this->watcherIds['idle_readable'])) {
                $this->reactor->cancel($this->watcherIds['idle_readable']);
                $this->watcherIds['idle_readable'] = null;
            }

            $this->scheduleProcessNextOp();
        }

        return $op;
    }

    private function processNextOp()
    {
        do {
            if (!$this->opQueue->count()) {
                $this->handleEmptyOpQueue();
                return;
            }

            $this->currentOp = $this->opQueue->dequeue();
            $this->trigger('op.dequeue', $this->currentOp);
        } while ($this->currentOp->cancelled);

        $callback = $this->currentOp->target;
        $this->currentOp->retVal = $callback(...$this->currentOp->args);
        $this->currentOp->executing = true;

        $this->currentOp->trigger('start', $this->currentOp);
        $this->trigger('op.start', $this->currentOp);

        $this->watcherIds['op_readable'] = $this->reactor->onReadable($this->pqConnection->socket, $this->callbacks['op_readable']);
    }

    /**
     * @param string $channel
     * @param string $event
     * @param mixed $args
     */
    private function triggerChannelEvent($channel, $event, ...$args)
    {
        $this->trigger('channel.' . $channel . '.' . $event, ...$args);
        $this->trigger('channel.' . $event, $channel, ...$args);
    }

    /**
     * @return bool
     */
    public function isConnected()
    {
        return $this->connected;
    }

    /**
     * @return bool
     */
    public function isIdle()
    {
        return $this->currentOp === null && !$this->opQueue->count();
    }

    /**
     * @return bool
     */
    public function getQueueCount()
    {
        return $this->opQueue->count();
    }

    /**
     * @return pqConnection
     */
    public function getPQConnection()
    {
        return $this->pqConnection;
    }

    /**
     * @return Reactor
     */
    public function getReactor()
    {
        return $this->reactor;
    }

    /**
     * @return string[]
     */
    public function getSubscribedChannels()
    {
        return $this->subscribedChannels;
    }

    /**
     * @param string $dsn
     * @param array $options
     */
    public function connect($dsn, array $options = [])
    {
        $flags = pqConnection::ASYNC;
        if (!empty($options[self::OPTION_PERSISTENT])) {
            $this->persistent = true;
            $flags |= pqConnection::PERSISTENT;
        }
        $this->pqConnection = new pqConnection($dsn, $flags);

        unset($options[self::OPTION_PERSISTENT]);
        foreach ($options + [self::OPTION_UNBUFFERED => true] as $option => $value) {
            $this->setOption($option, $value);
        }

        $this->reactor->onWritable($this->pqConnection->socket, $this->callbacks['connect_activity']);
    }

    /**
     * @param int|string $option
     * @param mixed $value
     */
    public function setOption($option, $value)
    {
        if (isset(self::$optionPropertyMap[$option])) {
            $this->pqConnection->{self::$optionPropertyMap[$option]} = $value;
            return;
        } else if (self::OPTION_PERSISTENT == $option) {
            throw new \InvalidArgumentException('Persistence can only be specified when connecting');
        } else if (in_array($option, self::$optionPropertyMap)) {
            $this->pqConnection->{$option} = $value;
            return;
        }

        throw new \InvalidArgumentException('Unknown option: ' . $option);
    }

    /**
     * @param int|string $option
     * @return mixed
     */
    public function getOption($option)
    {
        if (isset(self::$optionPropertyMap[$option])) {
            return $this->pqConnection->{self::$optionPropertyMap[$option]};
        } else if (self::OPTION_PERSISTENT == $option) {
            return $this->persistent;
        } else if (in_array($option, self::$optionPropertyMap)) {
            return $this->pqConnection->{$option};
        }

        throw new \InvalidArgumentException('Unknown option: ' . $option);
    }

    /**
     * @param string $name
     * @param int $flags
     * @param string $query
     * @param callable $callback
     */
    public function declareCursor($name, $flags, $query, callable $callback)
    {
        //todo
    }

    /**
     * @param string $sql
     * @return Query
     */
    public function exec($sql)
    {
        $method = [$this->pqConnection, 'execAsync'];
        $args = [$sql];

        return new Query($this, $this->enqueueOp($method, $args));
    }

    /**
     * @param string $sql
     * @param array $params
     * @param array $types
     * @return Query
     */
    public function execParams($sql, array $params, array $types = null)
    {
        $method = [$this->pqConnection, 'execParamsAsync'];
        $args = [$sql, $params, $types];

        return new Query($this, $this->enqueueOp($method, $args));
    }

    /**
     * @param string $channel
     */
    public function listen($channel)
    {
        if (isset($this->subscribedChannels[$channel])) {
            throw new \LogicException('Cannot listen to channel ' . $channel . ': already listening');
        }
        if (isset($this->pendingListenChannels[$channel])) {
            throw new \LogicException('Cannot listen to channel ' . $channel . ': LISTEN operation pending');
        }
        if (isset($this->pendingUnlistenChannels[$channel]) && !$this->pendingUnlistenChannels[$channel]->executing) {
            if ($this->pendingUnlistenChannels[$channel]->cancelled) {
                throw new \LogicException('Cannot listen to channel ' . $channel . ': already listening');
            }

            $this->pendingUnlistenChannels[$channel]->cancelled = true;
            return;
        }

        $method = [$this->pqConnection, 'listenAsync'];
        $args = [$channel, $this->callbacks['channel_message_handler']];
        $autoComplete = false;

        $this->pendingListenChannels[$channel] = $this->enqueueOp($method, $args, $autoComplete)
            ->on('ready', function() use ($channel) {
                $this->subscribedChannels[$channel] = $channel;
                unset($this->pendingListenChannels[$channel]);

                $this->triggerChannelEvent($channel, 'listen');
                $this->clearResultsAndCompleteCurrentOp();
            });
    }

    /**
     * @param string $channel
     * @param string $message
     */
    public function notify($channel, $message)
    {
        $method = [$this->pqConnection, 'notifyAsync'];
        $args = [$channel, $message];
        $autoComplete = false;

        $this->enqueueOp($method, $args, $autoComplete)
            ->on('ready', function() use ($channel, $message) {
                $this->triggerChannelEvent($channel, 'notify', $message);
                $this->clearResultsAndCompleteCurrentOp();
            });
    }

    /**
     * @param string $name
     * @param string $sql
     * @param array $types
     * @return Statement
     */
    public function prepare($name, $sql, array $types = null)
    {
        $method = [$this->pqConnection, 'prepareAsync'];
        $args = [$name, $sql, $types];

        return new Statement($this, $this->enqueueOp($method, $args));
    }

    public function reset()
    {
        $method = [$this->pqConnection, 'resetAsync'];
        $args = [];
        $autoComplete = true;

        $this->enqueueOp($method, $args, $autoComplete);
    }

    /**
     * @param int $isolation
     * @param bool $readonly
     * @param bool $deferrable
     * @return Transaction
     */
    public function startTransaction($isolation = null, $readonly = null, $deferrable = null)
    {
        $method = [$this->pqConnection, 'startTransactionAsync'];
        $args = [
            $isolation !== null ? (int)$isolation : $this->pqConnection->defaultTransactionIsolation,
            $readonly !== null ? (bool)$readonly : $this->pqConnection->defaultTransactionReadonly,
            $deferrable !== null ? (bool)$deferrable : $this->pqConnection->defaultTransactionDeferrable,
        ];

        return new Transaction($this, $this->enqueueOp($method, $args));
    }

    /**
     * @param string $channel
     */
    public function unlisten($channel)
    {
        if (!isset($this->subscribedChannels[$channel])) {
            throw new \LogicException('Cannot stop listening to channel ' . $channel . ': not listening');
        }
        if (isset($this->pendingUnlistenChannels[$channel])) {
            throw new \LogicException('Cannot stop listening to channel ' . $channel . ': UNLISTEN operation pending');
        }
        if (isset($this->pendingListenChannels[$channel]) && !$this->pendingListenChannels[$channel]->executing) {
            if ($this->pendingListenChannels[$channel]->cancelled) {
                throw new \LogicException('Cannot listen to channel ' . $channel . ': already listening');
            }

            $this->pendingListenChannels[$channel]->cancelled = true;
            return;
        }

        $method = [$this->pqConnection, 'unlistenAsync'];
        $args = [$channel];
        $autoComplete = false;

        $this->pendingUnlistenChannels[$channel] = $this->enqueueOp($method, $args, $autoComplete)
            ->on('ready', function() use ($channel) {
                unset($this->subscribedChannels[$channel], $this->pendingUnlistenChannels[$channel]);

                $this->triggerChannelEvent($channel, 'unlisten');
                $this->clearResultsAndCompleteCurrentOp();
            });
    }
}
