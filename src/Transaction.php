<?php

namespace pqAsync;

use Amp\Reactor;
use pq\Connection as pqConnection;
use pq\Transaction as pqTransaction;
use pq\Result as pqResult;

/**
 * Class Transaction
 *
 * @package pqAsync
 */
class Transaction
{
    use EventEmitter;
    use OpDelegate;

    const OPTION_ISOLATION = 1;
    const OPTION_READONLY = 2;
    const OPTION_DEFERRABLE = 3;

    const STATE_PENDING = 0;
    const STATE_STARTING = 1;
    const STATE_OPEN = 2;
    const STATE_CLOSING = 3;
    const STATE_CLOSED = 4;
    const STATE_FAILED = 5;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var Op
     */
    private $rootOp;

    /**
     * @var Reactor
     */
    private $reactor;

    /**
     * @var pqConnection
     */
    private $pqConnection;

    /**
     * @var pqTransaction
     */
    private $pqTransaction;

    /**
     * @var int
     */
    private $state = self::STATE_PENDING;

    /**
     * @var array
     */
    private $opWatcherIds = [];

    /**
     * @var Op[]|\SplQueue
     */
    private $containedOps;

    /**
     * @var string[]
     */
    private static $optionPropertyMap = [
        self::OPTION_ISOLATION  => 'isolation',
        self::OPTION_READONLY   => 'readonly',
        self::OPTION_DEFERRABLE => 'deferrable',
    ];

    /**
     * @param Connection $connection
     * @param Op $op
     */
    public function __construct(Connection $connection, Op $op)
    {
        $this->connection = $connection;
        $this->rootOp = $op;

        $this->reactor = $connection->getReactor();
        $this->pqConnection = $connection->getPQConnection();

        $this->containedOps = new \SplQueue;

        $this->registerCallbacks();
    }

    private function registerCallbacks()
    {
        $this->opWatcherIds['enqueue'] = $this->connection->on('op.enqueue', function(Op $op) {
            if ($this->isContainableOp($op)) {
                $this->containedOps->enqueue($op);
            }
        });

        $this->rootOp->on('start', function(Op $op) {
            $this->state = self::STATE_STARTING;
            $this->pqTransaction = $op->retVal;

            $this->opWatcherIds['dequeue'] = $this->connection->on('op.dequeue', function(Op $op) {
                if ($this->isContainableOp($op)) {
                    // todo: remove temporary debugging check
                    if ($this->containedOps->dequeue($op) !== $op) {
                        throw new \LogicException('Bottom of list is not the same as the currently executing op!');
                    }
                }
            });
        });

        $this->rootOp->on('ready', function(Op $op) {
            $result = $this->pqConnection->getResult();

            if (pqResult::COMMAND_OK === $result->status) {
                $this->state = self::STATE_OPEN;
                $this->trigger('start.success');
            } else {
                $this->state = self::STATE_FAILED;
                $this->unregisterOpWatchers();
                $this->trigger('start.fail', new \Exception($result->errorMessage));
            }

            $this->clearResultsAndCompleteOp($this->pqConnection, $op);
        });
    }

    private function unregisterOpWatchers()
    {
        $this->connection->off('enqueue');
        $this->connection->off('dequeue');
    }

    /**
     * @param Op $op
     * @return bool
     */
    private function isContainableOp(Op $op)
    {
        return is_array($op->target) && substr($op->target, 0, 4) === 'exec';
    }

    /**
     * @param string $eventPrefix
     */
    private function checkCommandOK($eventPrefix)
    {
        $result = $this->pqConnection->getResult();

        if (pqResult::COMMAND_OK === $result->status) {
            $this->trigger($eventPrefix . '.success');
        } else {
            $this->trigger($eventPrefix . '.fail', new \Exception($result->errorMessage));
        }
    }

    /**
     * @return Connection
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @return pqTransaction
     */
    public function getPQTransaction()
    {
        return $this->pqTransaction;
    }

    /**
     * @param int|string $option
     * @param mixed $value
     */
    public function setOption($option, $value)
    {
        if (isset(self::$optionPropertyMap[$option])) {
            $this->pqTransaction->{self::$optionPropertyMap[$option]} = $value;
            return;
        } else if (in_array($option, self::$optionPropertyMap)) {
            $this->pqTransaction->{$option} = $value;
            return;
        }

        throw new \InvalidArgumentException('Unknown option: ' . $option);
    }

    /**
     * @param int|string $option
     * @return mixed
     */
    public function getOption($option)
    {
        if (isset(self::$optionPropertyMap[$option])) {
            return $this->pqTransaction->{self::$optionPropertyMap[$option]};
        } else if (in_array($option, self::$optionPropertyMap)) {
            return $this->pqTransaction->{$option};
        }

        throw new \InvalidArgumentException('Unknown option: ' . $option);
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    public function commit()
    {
        if ($this->state > self::STATE_OPEN) {
            throw new \LogicException('Cannot commit transaction: not open');
        }
        $this->state = self::STATE_CLOSING;

        $this->unregisterOpWatchers();

        $method = [$this->pqTransaction, 'commitAsync'];
        $args = [];
        $autoComplete = false;

        $this->enqueueOp($this->rootOp, $method, $args, $autoComplete)
            ->on('ready', function(Op $op) {
                $this->state = self::STATE_CLOSED;
                $this->checkCommandOK('commit');
                $this->clearResultsAndCompleteOp($this->pqConnection, $op);
            });
    }

    public function createSavepoint()
    {
        if ($this->state > self::STATE_OPEN) {
            throw new \LogicException('Cannot create savepoint: transaction not open');
        }

        $method = [$this->pqTransaction, 'savepointAsync'];
        $args = [];
        $autoComplete = false;

        $this->enqueueOp($this->rootOp, $method, $args, $autoComplete)
            ->on('ready', function(Op $op) {
                $this->checkCommandOK('savepoint.create');
                $this->clearResultsAndCompleteOp($this->pqConnection, $op);
            });
    }

    /**
     * @param callable $callback
     */
    public function exportSnapshot(callable $callback)
    {
        if ($this->state > self::STATE_OPEN) {
            throw new \LogicException('Cannot export snapshot: transaction not open');
        }

        $method = [$this->pqTransaction, 'exportSnapshotAsync'];
        $args = [];
        $autoComplete = false;

        $this->enqueueOp($this->rootOp, $method, $args, $autoComplete)
            ->on('ready', function(Op $op) use($callback) {
                $result = $this->pqConnection->getResult();

                if ($result->status === pqResult::TUPLES_OK) {
                    $result->fetchCol($name);
                    $callback(true, $name);
                    $this->trigger('snapshot.export.success', $name);
                } else {
                    $e = new \Exception($result->errorMessage);
                    $callback(false, $e);
                    $this->trigger('snapshot.export.fail', $e);
                }

                $this->clearResultsAndCompleteOp($this->pqConnection, $op);
            });
    }

    /**
     * @param string $snapshotId
     */
    public function importSnapshot($snapshotId)
    {
        if ($this->state > self::STATE_OPEN) {
            throw new \LogicException('Cannot import snapshot: transaction not open');
        }

        $method = [$this->pqTransaction, 'importSnapshotAsync'];
        $args = [$snapshotId];
        $autoComplete = false;

        $this->enqueueOp($this->rootOp, $method, $args, $autoComplete)
            ->on('ready', function(Op $op) {
                $this->checkCommandOK('snapshot.import');
                $this->clearResultsAndCompleteOp($this->pqConnection, $op);
            });
    }

    public function rollback()
    {
        if ($this->state > self::STATE_OPEN) {
            throw new \LogicException('Cannot roll back transaction: not open');
        }
        $this->state = self::STATE_CLOSING;

        foreach ($this->containedOps as $op) {
            $op->cancelled = true;
        }
        $this->unregisterOpWatchers();

        $method = [$this->pqTransaction, 'rollbackAsync'];
        $args = [];
        $autoComplete = false;

        $this->enqueueOp($this->rootOp, $method, $args, $autoComplete)
            ->on('ready', function(Op $op) {
                $this->state = self::STATE_CLOSED;
                $this->checkCommandOK('rollback');
                $this->clearResultsAndCompleteOp($this->pqConnection, $op);
            });
    }
}
