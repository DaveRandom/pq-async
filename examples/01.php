<?php

namespace pqAsync;

use Amp\NativeReactor;
use pq\Result as pqResult;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/config.php';

(new NativeReactor)->run(function($reactor) {

    $dsn = sprintf('postgres://%s:%s@%s/%s', PGSQL_USER, PGSQL_PASS, PGSQL_HOST, PGSQL_DBNAME);
    $options = [
        'defaultFetchType' => pqResult::FETCH_ASSOC,
    ];

    $conn = new Connection($reactor, $dsn, $options);
    $conn->on('*', function ($name) {
        echo "Connection event emitted: $name\n";
    });
    $conn->on('connect.fail', function(\Exception $e) {
        echo $e;
    });
    $conn->on('connect.success', function(Connection $conn) {
        $sql = "
            SELECT id FROM shared.customers ORDER BY id LIMIT 5;
            SELECT id FROM shared.users ORDER BY id LIMIT 5;
        ";

        $query = $conn->exec($sql);
        $query->on('*', function ($name) {
            echo "Query event emitted: $name\n";
        });
        $query->on('resultset.row', function (pqResult $result) {
            foreach ($result as $row) {
                echo "Got row: {$row['id']}\n";
            }
        });
        unset($query);

        $stmt = $conn->prepare('test', "
            SELECT id FROM shared.customers WHERE id > $1 ORDER BY id LIMIT 5;
        ");
        $stmt->on('*', function ($name) {
            echo "Stmt test event emitted: $name\n";
        });
        $stmt->on('prepare.success', function(Statement $stmt) use($sql) {
            $stmt->describe(function($oids) {
                var_dump($oids);
            });

            $query = $stmt->exec([10005]);
            $query->on('*', function ($name) {
                echo "Query event emitted: $name\n";
            });
            $query->on('resultset.row', function (pqResult $result) {
                foreach ($result as $row) {
                    echo "Got row: {$row['id']}\n";
                }
            });
            $query->on('response.end', function () use ($stmt, $sql) {
                $query = $stmt->exec([10010]);
                $query->on('*', function ($name) {
                    echo "Query event emitted: $name\n";
                });
                $query->on('resultset.row', function (pqResult $result) {
                    foreach ($result as $row) {
                        echo "Got row: {$row['id']}\n";
                    }
                });
                $query->on('response.end', function () use ($stmt, $sql) {
                    $query = $stmt->getConnection()->exec($sql);
                    $query->on('*', function ($name) {
                        echo "Query event emitted: $name\n";
                    });
                    $query->on('resultset.row', function (pqResult $result) {
                        foreach ($result as $row) {
                            echo "Got row: {$row['id']}\n";
                        }
                    });
                });
            });
        });
    });
});
