<?php

namespace pqAsync;

use Amp\NativeReactor;
use Amp\Reactor;
use pq\Result as pqResult;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/config.php';

(new NativeReactor)->run(function(Reactor $reactor) {
    $dsn = sprintf('postgres://%s:%s@%s/%s', PGSQL_USER, PGSQL_PASS, PGSQL_HOST, PGSQL_DBNAME);
    $options = [
        'defaultFetchType' => pqResult::FETCH_ASSOC,
    ];

    $conn1 = new Connection($reactor, $dsn, $options);
    $conn2 = new Connection($reactor, $dsn, $options);

    $conn1->on('*', function ($name) {
        echo "Connection 1 event emitted: $name\n";
    });
    $conn2->on('*', function ($name) {
        echo "Connection 2 event emitted: $name\n";
    });

    $onReady = function() use($conn1, $conn2) {
        $conn1->listen('test');
        $conn1->on('channel.test.listen', function() use($conn2) {
            $conn2->notify('test', '1');
            $conn2->notify('test', '2');
            $conn2->notify('test', '3');
        });
        $conn1->on('channel.test.message', function($message, $srcPID, Connection $conn) {
            echo "Got message: {$message} from PID $srcPID\n";

            if ($message > 1) {
                $query = $conn->exec("SELECT id FROM shared.users ORDER BY id LIMIT 5");
                $query->on('*', function ($name) {
                    echo "Query event emitted: $name\n";
                });
                $query->on('resultset.row', function (pqResult $result) {
                    foreach ($result as $row) {
                        echo "Got row: {$row['id']}\n";
                    }
                });
                if ($message > 2) {
                    $query->on('response.end', function () use ($conn) {
                        $conn->unlisten('test');
                    });
                }
            }
        });
    };

    $onConnect = function() use($onReady) {
        static $connected = 0;
        if (++$connected === 2) {
            $onReady();
        }
    };
    $conn1->on('connect.success', $onConnect);
    $conn2->on('connect.success', $onConnect);
    $conn1->on('connect.fail', function($e) { echo $e; });
    $conn2->on('connect.fail', function($e) { echo $e; });
});
