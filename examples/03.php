<?php

namespace pqAsync;

use Amp\NativeReactor;
use Amp\Reactor;
use pq\Result as pqResult;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/config.php';

(new NativeReactor)->run(function(Reactor $reactor) {
    $dsn = sprintf('postgres://%s:%s@%s/%s', PGSQL_USER, PGSQL_PASS, PGSQL_HOST, PGSQL_DBNAME);
    $options = [
        'defaultFetchType' => pqResult::FETCH_ASSOC,
    ];

    $onError = function($e) { echo $e; };

    $conn = new Connection($reactor, $dsn, $options);
    $conn->on('connect.fail', $onError);

    $conn->on('connect.success', function() use($conn, $onError) {
        $transaction = $conn->startTransaction();
        $transaction->on('start.fail', $onError);

        $transaction->on('start.success', function() use($conn, $transaction) {
            $transaction->exportSnapshot(function($success, $result) {
                var_dump($success);
                echo $result . "\n";
            });

            $conn->exec('INSERT INTO shared.rejection_reasons (name) VALUES(\'test 1\')');
            $transaction->rollback();
            $conn->exec('INSERT INTO shared.rejection_reasons (name) VALUES(\'test 2\')');
        });
    });
});
